#include <stdio.h>
#include <math.h>

float Suma(float a, float b);
float Resta(float a, float b);

int main(void)
{
    float a,b, suma, resta;
    a=6;
    b=2;
    suma=Suma(a,b);
    resta=Resta(a,b);
    printf("Suma: %.2f + %.2f = %.2f\n", a,b,suma);
    printf("Resta: %.2f - %.2f = %.2f", a,b,resta);
   

    return 0;
}

float Suma(float a, float b)
{
    float suma;
    suma=a+b;

    return suma; 
}

float Resta(float a, float b)
{
    float resta; 
    resta=a-b;

    return resta;
}